# logback-sample01

SLF4J + Logback のサンプル。
VSCode + devcontainer を使った開発を想定

- [Logback](https://logback.qos.ch/) : ログ出力ライブラリ。SLF4J から使われること前提のログ出力実装
- [SLF4J](https://www.slf4j.org/) : ログファサードライブラリ。各ログ出力実装を統合する I/F
- Java / Maven の Docker イメージ : https://hub.docker.com/_/maven


## 設定ファイルの作成

参考 : https://maven.apache.org/settings.html

```
mkdir -p maven/.m2

tee maven/.m2/settings.xml <<EOF > /dev/null
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository/>
  <interactiveMode/>
  <offline/>
  <pluginGroups/>
  <servers/>
  <mirrors/>
  <proxies/>
  <profiles/>
  <activeProfiles/>
</settings>
EOF
```


## プロジェクト作成

シンプル構成の Maven プロジェクトで開始する。

参考 : 

- https://maven.apache.org/archetype/index.html
- https://maven.apache.org/archetype/maven-archetype-plugin/index.html
- https://maven.apache.org/archetype/maven-archetype-plugin/generate-mojo.html
- https://maven.apache.org/archetypes/
- https://maven.apache.org/archetypes/maven-archetype-simple/
- https://maven.apache.org/archetype/maven-archetype-plugin/examples/generate-batch.html

```
mkdir -p project

docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/project":/project \
  -v "$(pwd)/maven":/maven \
  -w /project \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    mvn archetype:generate \
      -Duser.home=/maven \
      -DinteractiveMode=false \
      -DarchetypeGroupId=org.apache.maven.archetypes \
      -DarchetypeArtifactId=maven-archetype-simple \
      -DarchetypeVersion=1.4 \
      -DgroupId=sample \
      -DartifactId=prj01 \
      -Dversion=1.0-SNAPSHOT \
      -Dpackage=sample.prj01 \
      -DoutputDirectory=/project
```

## 公式 Docker イメージを使った動作確認

mvn コマンドなどの動作確認用に公式の Docker イメージを使ったコンテナにアタッチして作業する場合

```
docker run \
  -it --rm \
  -u 1000:1000 \
  -v "$(pwd)/project/prj01":/project/prj01 \
  -v "$(pwd)/maven":/maven \
  -w /project/prj01 \
  -e MAVEN_CONFIG=/maven/.m2 \
  maven:3.9 \
    bash
```

実行コマンド例（パッケージ作成コマンド）

```
mvn clean package -Duser.home=/maven
```


## pom.xml の編集

事前に pom.xml の設計をしておき、それに合わせた編集を行う。
ここでは以下の設計とする。

- Java11 を利用
  - 参考 : https://maven.apache.org/guides/getting-started/maven-in-five-minutes.cgi#java-9-or-later
- logback-classic パッケージを利用
  - 参考 : https://logback.qos.ch/setup.html#mavenBuild
- logback 用設定ファイルを参照できるようにするため、 `resources` に設定ファイル用ディレクトリを追加

```
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.release>11</maven.compiler.release>
  </properties>
     
  <dependencies>
    ～略～
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.5.3</version>
    </dependency>

  </dependencies>

  <build>
    ～略～
    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        ～略～
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.1</version>
        </plugin>
        ～略～
      </plugins>
    </pluginManagement>

    <resources>
      <resource>
        <directory>src/main/config</directory>
      </resource>
    </resources>

  </build>
```


## .devcontainer / Dockerfile の作成

- 実行ユーザや Maven リポジトリなどに注意して各ファイルを作成
- Dockerfile は事前に設計した maven パッケージをインストールしたイメージを作成して利用する想定
  - ここでは、 `mvn package` でインストールされる範囲とする。
    - 実際に開発作業してたらプラグインなど他にも色々必要になりそう
    - dependency プラグインである程度解消できるが、イメージ作成後に一切ダウンロードが不要になる方法は未だ調査中
  - DevOps するなら `mvn deploy` まで含めて考える必要がある？


## コンテナ内での開発作業

コンテナ内の作業用ディレクトリでは以下のコマンドでビルドや実行などができる。
（ VSCode の Maven プラグインを使った作業も可）

```
# クリーンしてビルド（ jar 作成）
mvn clean package

# main の実行
## 参考 : https://www.mojohaus.org/exec-maven-plugin/usage.html
mvn exec:java -Dexec.mainClass="sample.prj01.App"
```


## logback.xml の作成

ログ出力の方法に合わせて設定ファイル logback.xml を作成する。
クラスパスを通すために pom.xml の `resources` に記載した場所に格納する。
