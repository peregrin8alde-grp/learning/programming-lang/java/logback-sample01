package sample.prj01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class App {
    final static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        logger.info("info msg");
        logger.error("error msg");
        logger.debug("debug msg");
        System.out.println( "Hello World!" );
    }
}
